require 'test_helper'

class StudySubjectsControllerTest < ActionController::TestCase
  setup do
    @study_subject = study_subjects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:study_subjects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create study_subject" do
    assert_difference('StudySubject.count') do
      post :create, study_subject: { title: @study_subject.title }
    end

    assert_redirected_to study_subject_path(assigns(:study_subject))
  end

  test "should show study_subject" do
    get :show, id: @study_subject
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @study_subject
    assert_response :success
  end

  test "should update study_subject" do
    patch :update, id: @study_subject, study_subject: { title: @study_subject.title }
    assert_redirected_to study_subject_path(assigns(:study_subject))
  end

  test "should destroy study_subject" do
    assert_difference('StudySubject.count', -1) do
      delete :destroy, id: @study_subject
    end

    assert_redirected_to study_subjects_path
  end
end
