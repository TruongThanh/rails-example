class Article < ActiveRecord::Base
  validates_presence_of :title, :body
  validates_presence_of :body
 belong_to :user
 has_and_belongs_to_many :categories
  def long_title
  "#{title} - #{published_at}"
  end
end
