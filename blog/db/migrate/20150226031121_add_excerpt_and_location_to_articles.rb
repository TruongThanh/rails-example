class AddExcerptAndLocationToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :excerpt, :string
    add_column :articles, :location, :string
    add_column :articles, :user_id, :integer
  end
end
