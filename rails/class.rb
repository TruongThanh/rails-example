class Student
def first_name=(value)
  @first_name = value
end

def first_name
  @first_name
end

def last_name=(value)
  @last_name = value
end

def last_name
  @last_name
end

def full_name
  last_name + ", " + first_name
end
end

@student = Student.new 
@student.first_name = "Bob"

@student.last_name = "Jones"
puts @student.full_name