array_test = [" first_line", " second_line", " third_line"]

for object in array_test
	puts "This is " + object
end

for surt in array_test.reverse
	print surt
end

count = 1

array_test.each do |item|
	puts "#{count} : #{item}"
	count += 1
end

array_test.each_with_index do |item.index|
	puts "#{index} : #{item}"
end