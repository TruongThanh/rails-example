class PronunciationsController < ApplicationController
  before_action :set_pronunciation, only: [:show, :edit, :update, :destroy]

  # GET /pronunciations
  # GET /pronunciations.json
  def index
    @pronunciations = Pronunciation.all
  end

  # GET /pronunciations/1
  # GET /pronunciations/1.json
  def show
  end

  # GET /pronunciations/new
  def new
    @pronunciation = Pronunciation.new
  end

  # GET /pronunciations/1/edit
  def edit
  end

  # POST /pronunciations
  # POST /pronunciations.json
  def create
    @pronunciation = Pronunciation.new(pronunciation_params)

    respond_to do |format|
      if @pronunciation.save
        format.html { redirect_to @pronunciation, notice: 'Pronunciation was successfully created.' }
        format.json { render :show, status: :created, location: @pronunciation }
      else
        format.html { render :new }
        format.json { render json: @pronunciation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pronunciations/1
  # PATCH/PUT /pronunciations/1.json
  def update
    respond_to do |format|
      if @pronunciation.update(pronunciation_params)
        format.html { redirect_to @pronunciation, notice: 'Pronunciation was successfully updated.' }
        format.json { render :show, status: :ok, location: @pronunciation }
      else
        format.html { render :edit }
        format.json { render json: @pronunciation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pronunciations/1
  # DELETE /pronunciations/1.json
  def destroy
    @pronunciation.destroy
    respond_to do |format|
      format.html { redirect_to pronunciations_url, notice: 'Pronunciation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pronunciation
      @pronunciation = Pronunciation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pronunciation_params
      params.require(:pronunciation).permit(:title)
    end
end
